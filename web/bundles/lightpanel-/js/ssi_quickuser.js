// JavaScript Document

function setupModal(action, username){
	
	
	document.getElementById("modalMessage").className = "";	
	
	
	username = username || false;	
	var modalInnerHTML, modalTitle, modalFooter;
	
	var okCancelButtons = 	"<a href=\"#\" class=\"btn\" data-dismiss=\"modal\">Fermer</a><a href=\"javascript:void()\" class=\"btn btn-primary\">Confirmer</a>";
	var okCUsersButtons = 	"<a href=\"#\" class=\"btn\" data-dismiss=\"modal\">Fermer</a>"+
							"<a href=\"javascript:void(0)\" onClick=\"quickUserAction('"+action+"', '"+username+"', document.getElementById('optn').value)\" class=\"btn btn-primary\">Confirmer</a>";
	var okCUsersButtonsNoParam = 	"<a href=\"#\" class=\"btn\" data-dismiss=\"modal\">Fermer</a>"+
							"<a href=\"javascript:void(0)\" onClick=\"quickUserAction('"+action+"', '"+username+"')\" class=\"btn btn-primary\">Confirmer</a>";
	
	var psayForm = 		"<form action=\"#\" name=\"messageForm\">"+
						"<p><label for=\"messageForm\">Saisissez la commande qui cliquez sur valider.</label>"+
						"<textarea name=\"message\"></textarea></p></form>";
	
	var sudoForm = 		"<form action=\"#\" name=\"commandForm\">"+
						"<p><label for=\"commandForm\">Saisissez la commande que vous souhaitez faire executer à "+username+".</label>"+
						"<input type=\"text\" name=\"command\"></p></form>";
	var sudoButtons = 	"<a href=\"#\" class=\"btn\" data-dismiss=\"modal\">Fermer</a>"+
						"<a href=\"javascript:void()\" class=\"btn btn-primary\">Exécuter</a>";
	
	var frebButtons = 	"<a href=\"#\" class=\"btn\" data-dismiss=\"modal\">Fermer</a>"+
						"<a href=\"#\" onClick=\"serverRestart()\" class=\"btn btn-success\">"+
						"<span class=\"icon-repeat\" style=\"margin-top:2px;\"></span>Redémarrer</a>";
	
	var innerHtmls = {	ban		: 	{
										title 	: 	"Confirmation avant bannissement.",
										message : 	"<div class=\"alert alert-warning\"><p>"+
										"<span class=\"label label-warning\" style=\"margin-bottom:4px;\">ATTENTION !</span><br />"+
										"L'utilisateur "+username+" se verra refuser l'accès au serveur {{server_ip}} de manière permanente.<br />"+
										"Vous devrez le débannir <b>manuellement</b>.</p></div>"+
										"<p>Voulez vous vraiment bannir l'utilisateur "+username+" ?</p>",
										footer 	: 	okCUsersButtonsNoParam,
										},
						kick	: 	{
										title 	: 	"Confirmation avant expulsion.",
										message : 	"Voulez vous vraiment kicker l'utilisateur "+username+" ?",
										footer 	: 	okCUsersButtonsNoParam,
										},
						jail	: 	{
										title 	: 	"Confirmation avant emprisonnement.",
										message : 	"Voulez vous vraiment emprisonner l'utilisateur "+username+" dans la cellule {{main_cell}} ?",
										footer 	: 	okCUsersButtonsNoParam,
										},
						psay	: 	{
										title 	: 	"Message personnel à "+username+".",
										message : 	psayForm,
										footer 	: 	okCUsersButtons,
										},
						sudo	: 	{
										title 	: 	"Commande sudo",
										message : 	sudoForm,
										footer 	: 	okCUsersButtons,
										},
						info    : 	{
										title 	: 	"Informations à propos de "+username+" :",
										message : 	"Quelques infos bien rangées",
										footer 	: 	"<a href=\"#\" class=\"btn btn-primary\" data-dismiss=\"modal\">Fermer</a>",
										},
						/***** FAST REBOOT ****/
						freb 	: 	{
										title 	: 	"Confirmation FastReboot :",
										message : 	"Souhaitez vous vraiment redémarrer ?",
										footer 	: 	frebButtons,
										},
						/************************/
						/***** GLOBAl RELOAD ****/
						greload	: 	{
										title 	: 	"Confirmation reload global :",
										message : 	"<div class=\"modal-body\"><div class=\"alert alert-error\"><p><span class=\"label label-important\" style=\"margin-bottom:4px;\">ATTENTION !</span><br />Un reload peut entraîner l'instabilité du serveur.<br />Notez aussi que les clients pour lesquels la session xAuth a expiré devront de nouveau se connecter.</p></div><p>Souhaitez vous vraiment recharger le serveur ?</p></div>",
										footer 	: 	okCancelButtons,
										}
						/************************/
						};
		
	
	document.getElementById("modalTitle").innerHTML		=innerHtmls[action]['title'];	
	document.getElementById("modalMessage").innerHTML	=innerHtmls[action]['message'];	
	document.getElementById("modalFooter").innerHTML	=innerHtmls[action]['footer'];	
}



jQuery(function($){
			
			$('.playerQuent_1Options').fadeOut();
			$('.playerplayernameOptions').fadeOut();
			$('.playerLawlMan444Options').fadeOut();
			$('.player__Solar__Options').fadeOut();
			$('.playerChristmoiOptions').fadeOut();
			$('.playerActoueOptions').fadeOut();
			
			/*var playercount = 5;
			var i =1; // si j'initialise avec 1, je mets <= dans le for.
			while(i<=playerCount){
				alert("Hey ");
				$('.player'+i).live('click', function(){
				$('.player'+i+'Options').slideToggle();})
				i+=1;
			}//*/
							
			$('.playerplayername').live('click', function(){
				$('.playerplayernameOptions').slideToggle();})
			$('.playerLawlMan444').live('click', function(){
				$('.playerLawlMan444Options').slideToggle();})	
			$('.player__Solar__').live('click', function(){
				$('.player__Solar__Options').slideToggle();})				
			$('.playerChristmoi').live('click', function(){
				$('.playerChristmoiOptions').slideToggle();})			
			$('.playerQuent_1').live('click', function(){
				$('.playerQuent_1Options').slideToggle();})			
			$('.playerActoue').live('click', function(){
				$('.playerActoueOptions').slideToggle();})
});