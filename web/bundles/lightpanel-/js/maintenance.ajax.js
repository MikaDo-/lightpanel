function broadcast(){
	
	var xhr = createXHR();
	var data = "consoleCommand=say [SSI Broadcast] : "+document.getElementById("broadcastMessage").value;
	
	document.getElementById("broadCastConfirmation").innerHTML = "Envoi du message global...";
	document.getElementById("broadCastConfirmation").className = "alert alert-warning";	
	
	xhr.onreadystatechange = function()
	{ 
		if(xhr.readyState == 4){
			if(xhr.status == 200){
				//document.getElementById(outputElement).innerHTML=xhr.responseXML;
				//updateOutputElement("output_div", xhr.responseXML);
				document.getElementById("broadCastConfirmation").innerHTML = "Message diffusé !";
				document.getElementById("broadCastConfirmation").className = "alert alert-success";
				
			}else{
				
				document.getElementById("broadCastConfirmation").innerHTML = "Erreur lors du broadcast !";
				document.getElementById("broadCastConfirmation").className = "alert alert-error";
			}	
		} 
	}; 
	
	xhr.open("POST", "http://teamsolex.eu/lc-ssi/ajax/ssi-panel.ajax.php", true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");  
	xhr.send(data);	
}

function serverRestart(){
	var xhr = createXHR();
	var data = "action=freb";
	
	document.getElementById("modalMessage").innerHTML = "Envoi de l'ordre de redémarrage...";
	document.getElementById("modalMessage").className = "alert alert-warning";	
	
	xhr.onreadystatechange = function()
	{ 
		if(xhr.readyState == 4){
			if(xhr.status == 200){
				document.getElementById("modalMessage").innerHTML = "Reboot effectué !";
				document.getElementById("modalMessage").className = "alert alert-success";
			}else{
				
				document.getElementById("modalMessage").innerHTML = "Erreur lors du reboot !";
				document.getElementById("modalMessage").className = "alert alert-error";
			}	
		} 
	}; 
	
	xhr.open("POST", "http://teamsolex.eu/lc-ssi/ajax/ssi-panel.ajax.php", true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");  
	xhr.send(data);		
}