// JavaScript Document



jQuery(function($){
			
			$('.maintenancePage').fadeIn();
			$('.consolePage').fadeOut();
			$('.permissionsPage').fadeOut();
			
			$('.maintBtn').addClass('btn-primary');
			
			$('.consoleBtn').live('click', function(){
				$('.permissionsPage').slideUp();
				$('.maintenancePage').slideUp();
				$('.consolePage').slideDown();
				
				document.getElementById("consoleCommand").focus();
				
				$('.permsBtn').removeClass('btn-primary');
				$('.maintBtn').removeClass('btn-primary');
				$('.consoleBtn').addClass('btn-primary');});
			
			$('.permsBtn').live('click', function(){
				$('.consolePage').slideUp();
				$('.maintenancePage').slideUp();
				$('.permissionsPage').slideDown();
				
				$('.permsBtn').addClass('btn-primary');
				$('.maintBtn').removeClass('btn-primary');
				$('.consoleBtn').removeClass('btn-primary');});
				
			$('.maintBtn').live('click', function(){
				$('.consolePage').slideUp();
				$('.permissionsPage').slideUp();
				$('.maintenancePage').slideDown();
				
				$('.permsBtn').removeClass('btn-primary');
				$('.maintBtn').addClass('btn-primary');
				$('.consoleBtn').removeClass('btn-primary');});
							
				
			// SOUS ÉLÉMENTS DE MAINTENANCE
			
			/* INIT */
			$('#otherTab').slideUp();
			$('#multiverseTab').slideUp();
			$('#xAuthTab').slideUp();
			$('#logBlockTab').slideUp();
			$('#pluginsTab').slideUp();
			$('#globalStateTab').slideDown();
			$('#globalStateButton').addClass("active");
			/* FIN INIT */
			$('#globalStateButton').live('click', function(){
				$('#otherTab').slideUp();
				$('#multiverseTab').slideUp();
				$('#logBlockTab').slideUp();
				$('#pluginsTab').slideUp();
				$('#globalStateTab').slideDown();
				$('#xAuthTab').slideUp();
				
				$('#globalStateButton').parent().addClass("active");
				$('#pluginsTabButton').parent().removeClass("active");
				$('#logBlockTabButton').parent().removeClass("active");
				$('#multiverseTabButton').parent().removeClass("active");
				$('#xAuthTabButton').parent().removeClass("active");
				$('#otherTabButton').parent().removeClass("active");
				});
				
			$('#pluginsTabButton').live('click', function(){
				$('#otherTab').slideUp();
				$('#multiverseTab').slideUp();
				$('#logBlockTab').slideUp();
				$('#globalStateTab').slideUp();
				$('#pluginsTab').slideDown();
				$('#xAuthTab').slideUp();
				
				$('#globalStateButton').parent().removeClass("active");
				$('#pluginsTabButton').parent().addClass("active");
				$('#logBlockTabButton').parent().removeClass("active");
				$('#multiverseTabButton').parent().removeClass("active");
				$('#xAuthTabButton').parent().removeClass("active");
				$('#otherTabButton').parent().removeClass("active");
				});
				
			$('#logBlockTabButton').live('click', function(){
				$('#otherTab').slideUp();
				$('#multiverseTab').slideUp();
				$('#globalStateTab').slideUp();
				$('#pluginsTab').slideUp();
				$('#logBlockTab').slideDown();
				$('#xAuthTab').slideUp();
				
				$('#globalStateButton').parent().removeClass("active");
				$('#pluginsTabButton').parent().removeClass("active");
				$('#logBlockTabButton').parent().addClass("active");
				$('#multiverseTabButton').parent().removeClass("active");
				$('#xAuthTabButton').parent().removeClass("active");
				$('#otherTabButton').parent().removeClass("active");
				});
				
			$('#multiverseTabButton').live('click', function(){
				$('#otherTab').slideUp();
				$('#globalStateTab').slideUp();
				$('#pluginsTab').slideUp();
				$('#logBlockTab').slideUp();
				$('#multiverseTab').slideDown();
				$('#xAuthTab').slideUp();
				
				$('#globalStateButton').parent().removeClass("active");
				$('#pluginsTabButton').parent().removeClass("active");
				$('#logBlockTabButton').parent().removeClass("active");
				$('#multiverseTabButton').parent().addClass("active");
				$('#xAuthTabButton').parent().removeClass("active");
				$('#otherTabButton').parent().removeClass("active");
				});
				
			$('#xAuthTabButton').live('click', function(){
				$('#otherTab').slideUp();
				$('#globalStateTab').slideUp();
				$('#pluginsTab').slideUp();
				$('#logBlockTab').slideUp();
				$('#multiverseTab').slideUp();
				$('#xAuthTab').slideUp();
				$('#xAuthTab').slideDown();
				
				$('#globalStateButton').parent().removeClass("active");
				$('#pluginsTabButton').parent().removeClass("active");
				$('#logBlockTabButton').parent().removeClass("active");
				$('#multiverseTabButton').parent().removeClass("active");
				$('#xAuthTabButton').parent().addClass("active");
				$('#otherTabButton').parent().removeClass("active");
				});
				
			$('#otherTabButton').live('click', function(){
				$('#globalStateTab').slideUp();
				$('#pluginsTab').slideUp();
				$('#logBlockTab').slideUp();
				$('#multiverseTab').slideUp();
				$('#xAuthTab').slideUp();
				$('#otherTab').slideDown();
				
				$('#globalStateButton').parent().removeClass("active");
				$('#pluginsTabButton').parent().removeClass("active");
				$('#logBlockTabButton').parent().removeClass("active");
				$('#multiverseTabButton').parent().removeClass("active");
				$('#xAuthTabButton').parent().removeClass("active");
				$('#otherTabButton').parent().addClass("active");
				});
				
				
			// SOUS ÉLÉMENTS DE MAINTENANCE
			
			/* INIT */
			$('#permissionsStateTab').slideDown();
			$('#permissionsUsersTab').slideUp();
			/*
			$('#').slideUp();
			$('#').slideUp();
			$('#').slideDown();//*/
			$('#permissionStateTab').addClass("active");
			/* FIN INIT */	
				
			$('#permissionsStateButton').live('click', function(){
				$('#permissionsUsersTab').slideUp();
				$('#permissionsStateTab').slideDown();
				
				$('#permissionsUsersButton').parent().removeClass("active");
				$('#permissionsStateButton').parent().addClass("active");
				});
			$('#permissionsUsersButton').live('click', function(){
				$('#permissionsUsersTab').slideDown();
				$('#permissionsStateTab').slideUp();
				
				$('#permissionsStateButton').parent().removeClass("active");
				$('#permissionsUsersButton').parent().addClass("active");
				});
				
});