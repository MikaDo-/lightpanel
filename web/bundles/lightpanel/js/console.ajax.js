// JavaScript Document


function createXHR()
{
   var request = false;
   try
   {
     request = new ActiveXObject('Msxml2.XMLHTTP');
   }
   catch (err2)
   {
   try {
      request = new ActiveXObject('Microsoft.XMLHTTP');
   }
   catch (err3) {
      try {  request = new XMLHttpRequest();}
      catch (err1) { request = false;}
    }
   }
  return request;
}

function loadLogs(){
	
	var xhr = createXHR();
	var data = "action=gl";
	
	document.getElementById("loadButton").innerHTML = "Chargement FTP";
	
	xhr.onreadystatechange = function()
	{ 
		if(xhr.readyState == 4){
			if(xhr.status == 200){
				//document.getElementById(outputElement).innerHTML=xhr.responseXML;
				//updateOutputElement("output_div", xhr.responseXML);
				document.getElementById("loadButton").innerHTML = "Logs rapatriés !";
				document.getElementById("loadButton").className = "btn btn-success";
				
			}else{
				
				document.getElementById("loadButton").innerHTML = "Erreur FTP !";
				document.getElementById("loadButton").className = "btn btn-danger";
			}	
		} 
	}; 
	
	xhr.open("POST", "lc-ssi/ajax/ssi-panel.ajax.php", true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");  
	xhr.send(data);
}


function execCmd()
{ 
	var command = document.getElementById("consoleCommand").value;
	var data="consoleCommand="+command;
	var xhr = createXHR();
	//var consoleCommand=uriEncodeComponent(command);
	
	xhr.onreadystatechange = function()
	{ 
		if(xhr.readyState == 4){
			if(xhr.status == 200){
				//document.getElementById(outputElement).innerHTML=xhr.responseXML;
				//updateOutputElement("output_div", xhr.responseXML);
				updateOutputElement("output_div", xhr.responseText);
			}else{
				//document.getElementById("output_div").innerHTML="Error: returned status code " + xhr.status + " " + xhr.statusText;
				alert("no !");
			}	
		} 
	}; 
	
	xhr.open("POST", "lc-ssi/ajax/ssi-panel.ajax.php", true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");  
	xhr.send(data);
	
} 


function consoleRefresh()
{ 
	var xhr = createXHR();
	var data="refreshConsole=1";
	
	document.getElementById("displayLogsButton").innerHTML = "Attente...";
	
	xhr.onreadystatechange = function()
	{ 
		if(xhr.readyState == 4){
			if(xhr.status == 200){
				//document.getElementById("output_div").innerHTML=xhr.responseText;
				//alert(xhr.responseText);
				updateOutputElement("output_div", xhr.responseText);
				document.getElementById("displayLogsButton").innerHTML = "Logs affich&eacute;s !";
				document.getElementById("displayLogsButton").className = "btn btn-success";
				
			}else{
				document.getElementById("output_div").innerHTML="Error: returned status code " + xhr.status + " " + xhr.statusText;
			}	
		} 
	}; 
	
	xhr.open("POST", "lc-ssi/ajax/ssi-panel.ajax.php", false);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");  
	xhr.send(data);
	
} 

function updateOutputElement(outputElement, textResponse){
	
	var currentContent = document.getElementById(outputElement).innerHTML;
	var node=textResponse.split('<EndOfLine>');
	var newContent=currentContent;
	var i = 0;
	for(i=0;i<node.length-1;i++)
	{
		newContent= newContent+"<br>"+node[i];
	}
	
	document.getElementById(outputElement).innerHTML = newContent;
}