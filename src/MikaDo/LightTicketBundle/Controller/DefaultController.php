<?php

namespace MikaDo\LightTicketBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('LightTicketBundle:Default:index.html.twig', array('name' => $name));
    }
}
