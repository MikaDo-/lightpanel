<?php

namespace MikaDo\LightTicketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightTicketBundle\Entity\SupportTicket
 *
 * @ORM\Table(name="lticket_supportticket")
 * @ORM\Entity(repositoryClass="MikaDo\LightTicketBundle\Entity\SupportTicketRepository")
 */
class SupportTicket
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="MikaDo\LightUserBundle\Entity\User")
     */
    private $owner;
    
    /**
     * @ORM\OneToMany(targetEntity="MikaDo\LightTicketBundle\Entity\TicketMessage", mappedBy="parentThread")
     */
    private $messages;

    /**
     * @var boolean $answered
     *
     * @ORM\Column(name="answered", type="boolean")
     */
    private $answered;

    /**
     * @var boolean $closed
     *
     * @ORM\Column(name="closed", type="boolean")
     */
    private $closed;

    /**
     * @var boolean $locked
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    private $locked;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answered
     *
     * @param boolean $answered
     * @return SupportTicket
     */
    public function setAnswered($answered)
    {
        $this->answered = $answered;
    
        return $this;
    }

    /**
     * Get answered
     *
     * @return boolean 
     */
    public function getAnswered()
    {
        return $this->answered;
    }

    /**
     * Set closed
     *
     * @param boolean $closed
     * @return SupportTicket
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;
    
        return $this;
    }

    /**
     * Get closed
     *
     * @return boolean 
     */
    public function getClosed()
    {
        return $this->closed;
    }

    /**
     * Set locked
     *
     * @param boolean $locked
     * @return SupportTicket
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;
    
        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean 
     */
    public function getLocked()
    {
        return $this->locked;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set owner
     *
     * @param MikaDo\LightUserBundle\Entity\User $owner
     * @return SupportTicket
     */
    public function setOwner(\MikaDo\LightUserBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;
    
        return $this;
    }

    /**
     * Get owner
     *
     * @return MikaDo\LightUserBundle\Entity\User 
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Add messages
     *
     * @param MikaDo\LightTicketBundle\Entity\TicketMessage $messages
     * @return SupportTicket
     */
    public function addMessage(\MikaDo\LightTicketBundle\Entity\TicketMessage $messages)
    {
        $this->messages[] = $messages;
    
        return $this;
    }

    /**
     * Remove messages
     *
     * @param MikaDo\LightTicketBundle\Entity\TicketMessage $messages
     */
    public function removeMessage(\MikaDo\LightTicketBundle\Entity\TicketMessage $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getMessages()
    {
        return $this->messages;
    }
}