<?php

namespace MikaDo\LightTicketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightTicketBundle\Entity\TicketMessage
 *
 * @ORM\Table(name="lticket_ticketmessage")
 * @ORM\Entity(repositoryClass="MikaDo\LightTicketBundle\Entity\TicketMessageRepository")
 */
class TicketMessage
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="MikaDo\LightTicketBundle\Entity\SupportTicket", inversedBy="messages")
     */
    private $parentThread;
    
    /**
     * @ORM\ManyToOne(targetEntity="MikaDo\LightUserBundle\Entity\User")
     */
    private $author;

    /**
     * @var string $message
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @var \DateTime $date
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var boolean $isRead
     *
     * @ORM\Column(name="isRead", type="boolean")
     */
    private $isRead;

    /**
     * @var boolean $deleted
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return TicketMessage
     */
    public function setMessage($message)
    {
        $this->message = $message;
    
        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return TicketMessage
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set isRead
     *
     * @param boolean $isRead
     * @return TicketMessage
     */
    public function setIsRead($isRead)
    {
        $this->isRead = $isRead;
    
        return $this;
    }

    /**
     * Get isRead
     *
     * @return boolean 
     */
    public function getIsRead()
    {
        return $this->isRead;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return TicketMessage
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set author
     *
     * @param MikaDo\LightUserBundle\Entity\User $author
     * @return TicketMessage
     */
    public function setAuthor(\MikaDo\LightUserBundle\Entity\User $author = null)
    {
        $this->author = $author;
    
        return $this;
    }

    /**
     * Get author
     *
     * @return MikaDo\LightUserBundle\Entity\User 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set parentThread
     *
     * @param MikaDo\LightTicketBundle\Entity\SupportTicket $parentThread
     * @return TicketMessage
     */
    public function setParentThread(\MikaDo\LightTicketBundle\Entity\SupportTicket $parentThread = null)
    {
        $this->parentThread = $parentThread;
    
        return $this;
    }

    /**
     * Get parentThread
     *
     * @return MikaDo\LightTicketBundle\Entity\SupportTicket 
     */
    public function getParentThread()
    {
        return $this->parentThread;
    }
}