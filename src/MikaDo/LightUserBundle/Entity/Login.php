<?php

namespace MikaDo\LightUserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightUserBundle\Entity\Login
 *
 * @ORM\Table(name="luser_login")
 * @ORM\Entity(repositoryClass="MikaDo\LightUserBundle\Entity\LoginRepository")
 */
class Login
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /*
     * @var \DateTime
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(name="ipAddress", type="string", length=255, unique=true)
     */
    private $ipAddress;

    /**
     * @ORM\ManyToOne(targetEntity="MikaDo\LightUserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return Login
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    
        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }


    /**
     * Set user
     *
     * @param MikaDo\LightUserBundle\Entity\User $user
     * @return Login
     */
    public function setUser(\MikaDo\LightUserBundle\Entity\User $user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return MikaDo\LightUserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set ipAddress
     *
     * @param string $ipAddress
     * @return Login
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;
    
        return $this;
    }

    /**
     * Get ipAddress
     *
     * @return string 
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }
}