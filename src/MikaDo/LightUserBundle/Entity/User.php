<?php

namespace MikaDo\LightUserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Symfony\Component\Security\Core\User\UserInterface;

/**
 * MikaDo\LightUserBundle\Entity\User
 *
 * @ORM\Table(name="luser_user")
 * @ORM\Entity(repositoryClass="MikaDo\LightUserBundle\Entity\UserRepository")
 */
class User implements \Symfony\Component\Security\Core\User\UserInterface
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(name="salt", type="string", length=255)
     */
    private $salt;
    
    /**
     * @ORM\Column(name="lastname", type="string")
     */
    private $lastname;
    
    /**
     * @ORM\Column(name="firstname", type="string")
     */
    private $firstname;
    
    /**
     * @ORM\Column(name="birthdate", type="datetime")
     */
    private $birthdate;
    
    /**
     * @ORM\Column(name="telephone", type="string", length=255)
     */
    private $telephone;
    
    /**
     * @ORM\Column(name="adresse", type="text")
     */
    private $address;
    
    /**
     * @ORM\Column(name="tokenCount", type="integer")
     */
    private $tokenCount;

    /**
     * @ORM\Column(name="roles", type="array")
     */
    private $roles;
    
    /**
     * @ORM\OneToMany(targetEntity="MikaDo\LightPanelBundle\Entity\Service", mappedBy="services")
     */
    private $services;

    public function __construct()
    {
        $this->roles = array();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;
        return $this;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function eraseCredentials()
    {
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    
        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    
        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     * @return User
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
    
        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime 
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return User
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    
        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set tokenCount
     *
     * @param integer $tokenCount
     * @return User
     */
    public function setTokenCount($tokenCount)
    {
        $this->tokenCount = $tokenCount;
    
        return $this;
    }

    /**
     * Get tokenCount
     *
     * @return integer 
     */
    public function getTokenCount()
    {
        return $this->tokenCount;
    }

    /**
     * Add services
     *
     * @param MikaDo\LightPanelBundle\Entity\Service $services
     * @return User
     */
    public function addService(\MikaDo\LightPanelBundle\Entity\Service $services)
    {
        $this->services[] = $services;
    
        return $this;
    }

    /**
     * Remove services
     *
     * @param MikaDo\LightPanelBundle\Entity\Service $services
     */
    public function removeService(\MikaDo\LightPanelBundle\Entity\Service $services)
    {
        $this->services->removeElement($services);
    }

    /**
     * Get services
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getServices()
    {
        return $this->services;
    }
}