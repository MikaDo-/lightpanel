<?php

namespace MikaDo\LightUserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LoginController extends Controller
{
    /*public function indexAction($name)
    {
        return $this->render('LightUserBundle:Default:index.html.twig', array('name' => $name));
    }*/
    public function loginAction() {
        return $this->render("LightUserBundle:login:login.html.twig");
    }
    
}
