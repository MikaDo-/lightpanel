<?php

namespace MikaDo\LightPanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightPanelBundle\Entity\Capacity
 *
 * @ORM\Table(name="lpanel_capacity")
 * @ORM\Entity(repositoryClass="MikaDo\LightPanelBundle\Entity\CapacityRepository")
 */
class Capacity
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="MikaDo\LightPanelBundle\Entity\Machine", inversedBy="capacities")
     */
    private $machine;
    
    /**
     * @ORM\OneToOne(targetEntity="MikaDo\LightSalesBundle\Entity\BuyableItem")
     */
    private $service;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set machine
     *
     * @param MikaDo\LightPanelBundle\Entity\Machine $machine
     * @return Capacity
     */
    public function setMachine(\MikaDo\LightPanelBundle\Entity\Machine $machine = null)
    {
        $this->machine = $machine;
    
        return $this;
    }

    /**
     * Get machine
     *
     * @return MikaDo\LightPanelBundle\Entity\Machine 
     */
    public function getMachine()
    {
        return $this->machine;
    }

    /**
     * Set service
     *
     * @param MikaDo\LightSalesBundle\Entity\BuyableItem $service
     * @return Capacity
     */
    public function setService(\MikaDo\LightSalesBundle\Entity\BuyableItem $service = null)
    {
        $this->service = $service;
    
        return $this;
    }

    /**
     * Get service
     *
     * @return MikaDo\LightSalesBundle\Entity\BuyableItem 
     */
    public function getService()
    {
        return $this->service;
    }
}