<?php

namespace MikaDo\LightPanelBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * MinecraftWorldRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MinecraftWorldRepository extends EntityRepository
{
}
