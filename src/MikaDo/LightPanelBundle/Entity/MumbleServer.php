<?php

namespace MikaDo\LightPanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightPanelBundle\Entity\MumbleServer
 *
 * @ORM\Table(name="lpanel_mumbleserver")
 * @ORM\Entity(repositoryClass="MikaDo\LightPanelBundle\Entity\MumbleServerRepository")
 */
class MumbleServer extends Service
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $slots
     *
     * @ORM\Column(name="slots", type="integer")
     */
    private $slots;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string $superUserPassword
     *
     * @ORM\Column(name="superUserPassword", type="string", length=255)
     */
    private $superUserPassword;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slots
     *
     * @param integer $slots
     * @return MumbleServer
     */
    public function setSlots($slots)
    {
        $this->slots = $slots;
    
        return $this;
    }

    /**
     * Get slots
     *
     * @return integer 
     */
    public function getSlots()
    {
        return $this->slots;
    }

    /**
     * Set superUserPassword
     *
     * @param string $superUserPassword
     * @return MumbleServer
     */
    public function setSuperUserPassword($superUserPassword)
    {
        $this->superUserPassword = $superUserPassword;
    
        return $this;
    }

    /**
     * Get superUserPassword
     *
     * @return string 
     */
    public function getSuperUserPassword()
    {
        return $this->superUserPassword;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MumbleServer
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
