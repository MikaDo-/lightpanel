<?php

namespace MikaDo\LightPanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightPanelBundle\Entity\MinecraftPlugin
 *
 * @ORM\Table(name="lpanel_minecraftplugin")
 * @ORM\Entity(repositoryClass="MikaDo\LightPanelBundle\Entity\MinecraftPluginRepository")
 */
class MinecraftPlugin extends Plugin
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $jarFile;
    
    /**
     * @ORM\ManyToMany(targetEntity="MikaDo\LightPanelBundle\Entity\MinecraftPlugin")
     */
    private $dependencies;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dependencies = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set jarFile
     *
     * @param string $jarFile
     * @return MinecraftPlugin
     */
    public function setJarFile($jarFile)
    {
        $this->jarFile = $jarFile;
    
        return $this;
    }

    /**
     * Get jarFile
     *
     * @return string 
     */
    public function getJarFile()
    {
        return $this->jarFile;
    }

    /**
     * Add dependencies
     *
     * @param MikaDo\LightPanelBundle\Entity\MinecraftPlugin $dependencies
     * @return MinecraftPlugin
     */
    public function addDependencie(\MikaDo\LightPanelBundle\Entity\MinecraftPlugin $dependencies)
    {
        $this->dependencies[] = $dependencies;
    
        return $this;
    }

    /**
     * Remove dependencies
     *
     * @param MikaDo\LightPanelBundle\Entity\MinecraftPlugin $dependencies
     */
    public function removeDependencie(\MikaDo\LightPanelBundle\Entity\MinecraftPlugin $dependencies)
    {
        $this->dependencies->removeElement($dependencies);
    }

    /**
     * Get dependencies
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getDependencies()
    {
        return $this->dependencies;
    }
}