<?php

namespace MikaDo\LightPanelBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * SourceServerRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SourceServerRepository extends EntityRepository
{
}
