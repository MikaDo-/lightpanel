<?php

namespace MikaDo\LightPanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightPanelBundle\Entity\MySQLDatabase
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="MikaDo\LightPanelBundle\Entity\MySQLDatabaseRepository")
 */
class MySQLDatabase
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;
    
    /**
     * @ORM\ManyToOne(targetEntity="MikaDo\LightPanelBundle\Entity\MySQLServer")
     */
    private $mysqlServer;
    
    /**
     * @ORM\OneToOne(targetEntity="MikaDo\LightPanelBundle\Entity\WebHosting", inversedBy="mysqlDatabase")
     */
    private $webHosting;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MySQLDatabase
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set mysqlServer
     *
     * @param MikaDo\LightPanelBundle\Entity\MySQLServer $mysqlServer
     * @return MySQLDatabase
     */
    public function setMysqlServer(\MikaDo\LightPanelBundle\Entity\MySQLServer $mysqlServer = null)
    {
        $this->mysqlServer = $mysqlServer;
    
        return $this;
    }

    /**
     * Get mysqlServer
     *
     * @return MikaDo\LightPanelBundle\Entity\MySQLServer 
     */
    public function getMysqlServer()
    {
        return $this->mysqlServer;
    }
    

    /**
     * Set webHosting
     *
     * @param MikaDo\LightPanelBundle\Entity\WebHosting $webHosting
     * @return MySQLDatabase
     */
    public function setWebHosting(\MikaDo\LightPanelBundle\Entity\WebHosting $webHosting = null)
    {
        $this->webHosting = $webHosting;
    
        return $this;
    }

    /**
     * Get webHosting
     *
     * @return MikaDo\LightPanelBundle\Entity\WebHosting 
     */
    public function getWebHosting()
    {
        return $this->webHosting;
    }
}