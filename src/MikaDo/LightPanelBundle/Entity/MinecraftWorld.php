<?php

namespace MikaDo\LightPanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightPanelBundle\Entity\MinecraftWorld
 *
 * @ORM\Table(name="lpanel_minecraftworld")
 * @ORM\Entity(repositoryClass="MikaDo\LightPanelBundle\Entity\MinecraftWorldRepository")
 */
class MinecraftWorld
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * @var string
     * @ORM\Column(name="folder", type="string", length=255)
     */
    private $folder;
    
    /**
     * @var string
     * @ORM\Column(name="type", type="integer")
     */
    private $type;
    
    /**
     * @ORM\ManyToMany(targetEntity="MikaDo\LightPanelBundle\Entity\MinecraftBackup", inversedBy="worlds")
     * @ORM\JoinColumn(nullable=false)
     */
    private $backups;
    
    /**
     * @ORM\ManyToOne(targetEntity="MikaDo\LightPanelBundle\Entity\MinecraftServer", inversedBy="worlds")
     */
    private $server;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MinecraftWorld
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set folder
     *
     * @param string $folder
     * @return MinecraftWorld
     */
    public function setFolder($folder)
    {
        $this->folder = $folder;
    
        return $this;
    }

    /**
     * Get folder
     *
     * @return string 
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return MinecraftWorld
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->backups = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add backups
     *
     * @param MikaDo\LightPanelBundle\Entity\MinecraftBackup $backups
     * @return MinecraftWorld
     */
    public function addBackup(\MikaDo\LightPanelBundle\Entity\MinecraftBackup $backups)
    {
        $this->backups[] = $backups;
    
        return $this;
    }

    /**
     * Remove backups
     *
     * @param MikaDo\LightPanelBundle\Entity\MinecraftBackup $backups
     */
    public function removeBackup(\MikaDo\LightPanelBundle\Entity\MinecraftBackup $backups)
    {
        $this->backups->removeElement($backups);
    }

    /**
     * Get backups
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getBackups()
    {
        return $this->backups;
    }
}