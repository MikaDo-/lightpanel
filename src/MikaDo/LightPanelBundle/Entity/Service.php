<?php

namespace MikaDo\LightPanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\MappedSuperclass */
abstract class Service
{    
    /**
     * @var \DateTime
     * @ORM\Column(name="expiration", type="datetime")
     */
    protected $expiration;
    
    /**
     * @var bool
     * @ORM\Column(name="active", type="boolean")
     */
    protected $active;
    
    /**
     * @ORM\ManyToOne(targetEntity="MikaDo\LightUserBundle\Entity\User", inversedBy="owner")
     */
    protected $owner;
    
    /**
     * @ORM\ManyToOne(targetEntity="MikaDo\LightPanelBundle\Entity\Machine", inversedBy="services")
     */
    protected $machine;
    
    public function getExpiration() {
        return $this->expiration;
    }

    public function setExpiration(\DateTime $expiration) {
        $this->expiration = $expiration;
    }

    public function getActive() {
        return $this->active;
    }

    public function setActive($active) {
        $this->active = $active;
    }

    public function getOwner() {
        return $this->owner;
    }

    public function setOwner($owner) {
        $this->owner = $owner;
    }





    
}