<?php

namespace MikaDo\LightPanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightPanelBundle\Entity\Machine
 *
 * @ORM\Table(name="lpanel_machine")
 * @ORM\Entity(repositoryClass="MikaDo\LightPanelBundle\Entity\MachineRepository")
 */
class Machine
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $alias;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ipAddress;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $ram;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cpu;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $connectionSpeed;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $raid;
    
    /**
     * @ORM\OneToMany(targetEntity="MikaDo\LightPanelBundle\Entity\Capacity", mappedBy="machine")
     */
    private $capacities;
    
    /**
     * @ORM\OneToMany(targetEntity="MikaDo\LightPanelBundle\Entity\Service", mappedBy="machine")
     */
    private $services;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Machine
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return Machine
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    
        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set ipAddress
     *
     * @param string $ipAddress
     * @return Machine
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;
    
        return $this;
    }

    /**
     * Get ipAddress
     *
     * @return string 
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set ram
     *
     * @param integer $ram
     * @return Machine
     */
    public function setRam($ram)
    {
        $this->ram = $ram;
    
        return $this;
    }

    /**
     * Get ram
     *
     * @return integer 
     */
    public function getRam()
    {
        return $this->ram;
    }

    /**
     * Set cpu
     *
     * @param string $cpu
     * @return Machine
     */
    public function setCpu($cpu)
    {
        $this->cpu = $cpu;
    
        return $this;
    }

    /**
     * Get cpu
     *
     * @return string 
     */
    public function getCpu()
    {
        return $this->cpu;
    }

    /**
     * Set connectionSpeed
     *
     * @param integer $connectionSpeed
     * @return Machine
     */
    public function setConnectionSpeed($connectionSpeed)
    {
        $this->connectionSpeed = $connectionSpeed;
    
        return $this;
    }

    /**
     * Get connectionSpeed
     *
     * @return integer 
     */
    public function getConnectionSpeed()
    {
        return $this->connectionSpeed;
    }

    /**
     * Set raid
     *
     * @param integer $raid
     * @return Machine
     */
    public function setRaid($raid)
    {
        $this->raid = $raid;
    
        return $this;
    }

    /**
     * Get raid
     *
     * @return integer 
     */
    public function getRaid()
    {
        return $this->raid;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->capacities = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add capacities
     *
     * @param MikaDo\LightPanelBundle\Entity\Capacity $capacities
     * @return Machine
     */
    public function addCapacity(\MikaDo\LightPanelBundle\Entity\Capacity $capacitiy)
    {
        $this->capacities[] = $capacitiy;
    
        return $this;
    }

    /**
     * Remove capacities
     *
     * @param MikaDo\LightPanelBundle\Entity\Capacity $capacities
     */
    public function removeCapacity(\MikaDo\LightPanelBundle\Entity\Capacity $capacity)
    {
        $this->capacities->removeElement($capacity);
    }

    /**
     * Get capacities
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCapacities()
    {
        return $this->capacities;
    }
}