<?php

namespace MikaDo\LightPanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightPanelBundle\Entity\WebHosting
 *
 * @ORM\Table(name="lpanel_webhosting")
 * @ORM\Entity(repositoryClass="MikaDo\LightPanelBundle\Entity\WebHostingRepository")
 */
class WebHosting extends Service
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="domain", type="string", length=255)
     */
    private $domain;
    
    /**
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;
    
    /**
     * @ORM\OneToOne(targetEntity="MikaDo\LightPanelBundle\Entity\MySQLDatabase", mappedBy="webHosting")
     */
    private $mysqlDatabase;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mysqlDatabase
     *
     * @param MikaDo\LightPanelBundle\MySQLDatabase $mysqlDatabase
     * @return WebHosting
     */
    public function setMysqlDatabase(\MikaDo\LightPanelBundle\MySQLDatabase $mysqlDatabase = null)
    {
        $this->mysqlDatabase = $mysqlDatabase;
    
        return $this;
    }

    /**
     * Get mysqlDatabase
     *
     * @return MikaDo\LightPanelBundle\MySQLDatabase 
     */
    public function getMysqlDatabase()
    {
        return $this->mysqlDatabase;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return WebHosting
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    
        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return WebHosting
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    
        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
}