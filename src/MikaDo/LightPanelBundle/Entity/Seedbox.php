<?php

namespace MikaDo\LightPanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightPanelBundle\Entity\Seedbox
 *
 * @ORM\Table(name="lpanel_seedbox")
 * @ORM\Entity(repositoryClass="MikaDo\LightPanelBundle\Entity\SeedboxRepository")
 */
class Seedbox extends Service
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $capacity;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $maxUpSpeed;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $maxDownSpeed;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $peerPort;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $RPCport;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $downloadsLimit;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $uploadsLimit;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set capacity
     *
     * @param integer $capacity
     * @return Seedbox
     */
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;
    
        return $this;
    }

    /**
     * Get capacity
     *
     * @return integer 
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * Set maxUpSpeed
     *
     * @param integer $maxUpSpeed
     * @return Seedbox
     */
    public function setMaxUpSpeed($maxUpSpeed)
    {
        $this->maxUpSpeed = $maxUpSpeed;
    
        return $this;
    }

    /**
     * Get maxUpSpeed
     *
     * @return integer 
     */
    public function getMaxUpSpeed()
    {
        return $this->maxUpSpeed;
    }

    /**
     * Set maxDownSpeed
     *
     * @param integer $maxDownSpeed
     * @return Seedbox
     */
    public function setMaxDownSpeed($maxDownSpeed)
    {
        $this->maxDownSpeed = $maxDownSpeed;
    
        return $this;
    }

    /**
     * Get maxDownSpeed
     *
     * @return integer 
     */
    public function getMaxDownSpeed()
    {
        return $this->maxDownSpeed;
    }

    /**
     * Set peerPort
     *
     * @param integer $peerPort
     * @return Seedbox
     */
    public function setPeerPort($peerPort)
    {
        $this->peerPort = $peerPort;
    
        return $this;
    }

    /**
     * Get peerPort
     *
     * @return integer 
     */
    public function getPeerPort()
    {
        return $this->peerPort;
    }

    /**
     * Set RPCport
     *
     * @param integer $rPCport
     * @return Seedbox
     */
    public function setRPCport($rPCport)
    {
        $this->RPCport = $rPCport;
    
        return $this;
    }

    /**
     * Get RPCport
     *
     * @return integer 
     */
    public function getRPCport()
    {
        return $this->RPCport;
    }

    /**
     * Set downloadsLimit
     *
     * @param integer $downloadsLimit
     * @return Seedbox
     */
    public function setDownloadsLimit($downloadsLimit)
    {
        $this->downloadsLimit = $downloadsLimit;
    
        return $this;
    }

    /**
     * Get downloadsLimit
     *
     * @return integer 
     */
    public function getDownloadsLimit()
    {
        return $this->downloadsLimit;
    }

    /**
     * Set uploadsLimit
     *
     * @param integer $uploadsLimit
     * @return Seedbox
     */
    public function setUploadsLimit($uploadsLimit)
    {
        $this->uploadsLimit = $uploadsLimit;
    
        return $this;
    }

    /**
     * Get uploadsLimit
     *
     * @return integer 
     */
    public function getUploadsLimit()
    {
        return $this->uploadsLimit;
    }
}