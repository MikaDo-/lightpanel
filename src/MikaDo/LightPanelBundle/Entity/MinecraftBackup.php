<?php

namespace MikaDo\LightPanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightPanelBundle\Entity\MinecraftBackup
 *
 * @ORM\Table(name="lpanel_minecraftbackup")
 * @ORM\Entity(repositoryClass="MikaDo\LightPanelBundle\Entity\MinecraftBackupRepository")
 */
class MinecraftBackup extends Backup
{    
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToMany(targetEntity="MikaDo\LightPanelBundle\Entity\MinecraftWorld", mappedBy="backups")
     * @ORM\JoinColumn(nullable=false)
     */
    private $worlds;
    
    /**
     * @ORM\ManyToOne(targetEntity="MikaDo\LightPanelBundle\Entity\MinecraftServer", inversedBy="backups")
     */
    private $server;
    
    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->worlds = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add worlds
     *
     * @param MikaDo\LightPanelBundle\Entity\MinecraftWorld $worlds
     * @return MinecraftBackup
     */
    public function addWorld(\MikaDo\LightPanelBundle\Entity\MinecraftWorld $worlds)
    {
        $this->worlds[] = $worlds;
    
        return $this;
    }

    /**
     * Remove worlds
     *
     * @param MikaDo\LightPanelBundle\Entity\MinecraftWorld $worlds
     */
    public function removeWorld(\MikaDo\LightPanelBundle\Entity\MinecraftWorld $worlds)
    {
        $this->worlds->removeElement($worlds);
    }

    /**
     * Get worlds
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorlds()
    {
        return $this->worlds;
    }
}