<?php

namespace MikaDo\LightPanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightPanelBundle\Entity\MySQLServer
 *
 * @ORM\Table(name="lpanel_mysqlserver")
 * @ORM\Entity(repositoryClass="MikaDo\LightPanelBundle\Entity\MySQLServerRepository")
 */
class MySQLServer
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="MikaDo\LightPanelBundle\Entity\Machine")
     */
    private $machine;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $port;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set port
     *
     * @param integer $port
     * @return MySQLServer
     */
    public function setPort($port)
    {
        $this->port = $port;
    
        return $this;
    }

    /**
     * Get port
     *
     * @return integer 
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Set machine
     *
     * @param MikaDo\LightPanelBundle\Entity\Machine $machine
     * @return MySQLServer
     */
    public function setMachine(\MikaDo\LightPanelBundle\Entity\Machine $machine = null)
    {
        $this->machine = $machine;
    
        return $this;
    }

    /**
     * Get machine
     *
     * @return MikaDo\LightPanelBundle\Entity\Machine 
     */
    public function getMachine()
    {
        return $this->machine;
    }
}