<?php

namespace MikaDo\LightPanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\MappedSuperclass */
class GameServer extends Service
{
    
    /**
     * @ORM\Column(type="integer")
     */
    private $port;
    
    /**
     * @ORM\ManyToOne(targetEntity="MikaDo\LightPanelBundle\Entity\Game")
     */
    private $game;


    /**
     * Set port
     *
     * @param integer $port
     * @return GameServer
     */
    public function setPort($port)
    {
        $this->port = $port;
    
        return $this;
    }

    /**
     * Get port
     *
     * @return integer 
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Set game
     *
     * @param MikaDo\LightPanelBundle\Game $game
     * @return GameServer
     */
    public function setGame(\MikaDo\LightPanelBundle\Game $game = null)
    {
        $this->game = $game;
    
        return $this;
    }

    /**
     * Get game
     *
     * @return MikaDo\LightPanelBundle\Game 
     */
    public function getGame()
    {
        return $this->game;
    }
}