<?php

namespace MikaDo\LightPanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightPanelBundle\Entity\Plugin
 *
 * @ORM\Table(name="lpanel_plugin")
 * @ORM\Entity(repositoryClass="MikaDo\LightPanelBundle\Entity\PluginRepository")
 */
class Plugin
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * Column(type="string", length=255)
     */
    private $name;
    
    /**
     * Column(type="string", length=255)
     */
    private $version;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}