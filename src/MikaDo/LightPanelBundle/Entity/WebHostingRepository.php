<?php

namespace MikaDo\LightPanelBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * WebHostingRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class WebHostingRepository extends EntityRepository
{
}
