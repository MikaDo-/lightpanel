<?php

namespace MikaDo\LightPanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightPanelBundle\Entity\MinecraftServer
 *
 * @ORM\Table(name="lpanel_minecraftserver")
 * @ORM\Entity(repositoryClass="MikaDo\LightPanelBundle\Entity\MinecraftServerRepository")
 */
class MinecraftServer extends GameServer
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\OneToMany(targetEntity="MikaDo\LightPanelBundle\Entity\MinecraftBackup", mappedBy="server")
     */
    private $backups;
    
    /**
     * @ORM\OneToMany(targetEntity="MikaDo\LightPanelBundle\Entity\MinecraftWorld", mappedBy="server")
     */
    private $worlds;
    
    /**
     * @ORM\ManyToMany(targetEntity="MikaDo\LightPanelBundle\Entity\MinecraftPlugin")
     */
    private $plugins;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $slots;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->backups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->worlds = new \Doctrine\Common\Collections\ArrayCollection();
        $this->plugins = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set slots
     *
     * @param integer $slots
     * @return MinecraftServer
     */
    public function setSlots($slots)
    {
        $this->slots = $slots;
    
        return $this;
    }

    /**
     * Get slots
     *
     * @return integer 
     */
    public function getSlots()
    {
        return $this->slots;
    }

    /**
     * Add backups
     *
     * @param MikaDo\LightPanelBundle\Entity\MinecraftBackup $backups
     * @return MinecraftServer
     */
    public function addBackup(\MikaDo\LightPanelBundle\Entity\MinecraftBackup $backups)
    {
        $this->backups[] = $backups;
    
        return $this;
    }

    /**
     * Remove backups
     *
     * @param MikaDo\LightPanelBundle\Entity\MinecraftBackup $backups
     */
    public function removeBackup(\MikaDo\LightPanelBundle\Entity\MinecraftBackup $backups)
    {
        $this->backups->removeElement($backups);
    }

    /**
     * Get backups
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getBackups()
    {
        return $this->backups;
    }

    /**
     * Add worlds
     *
     * @param MikaDo\LightPanelBundle\Entity\MinecraftWorld $worlds
     * @return MinecraftServer
     */
    public function addWorld(\MikaDo\LightPanelBundle\Entity\MinecraftWorld $worlds)
    {
        $this->worlds[] = $worlds;
    
        return $this;
    }

    /**
     * Remove worlds
     *
     * @param MikaDo\LightPanelBundle\Entity\MinecraftWorld $worlds
     */
    public function removeWorld(\MikaDo\LightPanelBundle\Entity\MinecraftWorld $worlds)
    {
        $this->worlds->removeElement($worlds);
    }

    /**
     * Get worlds
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getWorlds()
    {
        return $this->worlds;
    }

    /**
     * Add plugins
     *
     * @param MikaDo\LightPanelBundle\Entity\MinecraftPlugin $plugins
     * @return MinecraftServer
     */
    public function addPlugin(\MikaDo\LightPanelBundle\Entity\MinecraftPlugin $plugins)
    {
        $this->plugins[] = $plugins;
    
        return $this;
    }

    /**
     * Remove plugins
     *
     * @param MikaDo\LightPanelBundle\Entity\MinecraftPlugin $plugins
     */
    public function removePlugin(\MikaDo\LightPanelBundle\Entity\MinecraftPlugin $plugins)
    {
        $this->plugins->removeElement($plugins);
    }

    /**
     * Get plugins
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPlugins()
    {
        return $this->plugins;
    }
}