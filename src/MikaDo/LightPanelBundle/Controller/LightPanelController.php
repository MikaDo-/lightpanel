<?php

namespace MikaDo\LightPanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LightPanelController extends Controller
{
    public function overviewAction(){
        return new \Symfony\Component\HttpFoundation\Response("go <a href=\"http://lightpanel.com/games/minecraft-0\">http://lightpanel.com/games/minecraft-0</a>");
    }
}
