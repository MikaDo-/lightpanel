<?php

namespace MikaDo\LightPanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('LightPanelBundle:MinecraftServer:overview.html.twig');
    }
    public function testAction()
    {
        return $this->render('LightPanelBundle::test.html.twig');
    }
}
