<?php

namespace MikaDo\LightPanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MinecraftServerController extends Controller
{
    public function overviewAction($id){
        return $this->render("LightPanelBundle:MinecraftServer:overview.html.twig");
    }
}
