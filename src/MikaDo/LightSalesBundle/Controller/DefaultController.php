<?php

namespace MikaDo\LightSalesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('LightSalesBundle:Default:index.html.twig', array('name' => $name));
    }
}
