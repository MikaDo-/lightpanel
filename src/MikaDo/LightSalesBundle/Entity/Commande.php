<?php

namespace MikaDo\LightSalesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightSalesBundle\Entity\Commande
 *
 * @ORM\Table(name="lsales_commande")
 * @ORM\Entity(repositoryClass="MikaDo\LightSalesBundle\Entity\CommandeRepository")
 */
class Commande
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToMany(targetEntity="MikaDo\LightSalesBundle\Entity\BuyableItem")
     */
    private $items;
    
    /**
     * @ORM\ManyToOne(targetEntity="MikaDo\LightUserBundle\Entity\User")
     */
    private $emitter;
    
    /**
     * @ORM\ManyToOne(targetEntity="MikaDo\LightSalesBundle\Entity\Voucher")
     */
    private $voucher;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $paid;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $canceled;
    
    /**
     * @var \DateTime $date
     * @ORM\Column(type="datetime")
     */
    private $date;
    
    /**
     * @ORM\OneToOne(targetEntity="MikaDo\LightSalesBundle\Entity\Payment", mappedBy="commande")
     */
    private $payment;
    
    /**
     * @ORM\OneToOne(targetEntity="MikaDo\LightSalesBundle\Entity\Invoice", inversedBy="commande")
     */
    private $invoide;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set paid
     *
     * @param boolean $paid
     * @return Commande
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;
    
        return $this;
    }

    /**
     * Get paid
     *
     * @return boolean 
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set canceled
     *
     * @param boolean $canceled
     * @return Commande
     */
    public function setCanceled($canceled)
    {
        $this->canceled = $canceled;
    
        return $this;
    }

    /**
     * Get canceled
     *
     * @return boolean 
     */
    public function getCanceled()
    {
        return $this->canceled;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Commande
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Add items
     *
     * @param MikaDo\LightSalesBundle\Entity\BuyableItems $items
     * @return Commande
     */
    public function addItem(\MikaDo\LightSalesBundle\Entity\BuyableItems $items)
    {
        $this->items[] = $items;
    
        return $this;
    }

    /**
     * Remove items
     *
     * @param MikaDo\LightSalesBundle\Entity\BuyableItems $items
     */
    public function removeItem(\MikaDo\LightSalesBundle\Entity\BuyableItems $items)
    {
        $this->items->removeElement($items);
    }

    /**
     * Get items
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set emitter
     *
     * @param MikaDo\LightUserBundle\Entity\User $emitter
     * @return Commande
     */
    public function setEmitter(\MikaDo\LightUserBundle\Entity\User $emitter = null)
    {
        $this->emitter = $emitter;
    
        return $this;
    }

    /**
     * Get emitter
     *
     * @return MikaDo\LightUserBundle\Entity\User 
     */
    public function getEmitter()
    {
        return $this->emitter;
    }

    /**
     * Set voucher
     *
     * @param MikaDo\LightSalesBundle\Entity\Voucher $voucher
     * @return Commande
     */
    public function setVoucher(\MikaDo\LightSalesBundle\Entity\Voucher $voucher = null)
    {
        $this->voucher = $voucher;
    
        return $this;
    }

    /**
     * Get voucher
     *
     * @return MikaDo\LightSalesBundle\Entity\Voucher 
     */
    public function getVoucher()
    {
        return $this->voucher;
    }

    /**
     * Set payment
     *
     * @param MikaDo\LightSalesBundle\Entity\Payment $payment
     * @return Commande
     */
    public function setPayment(\MikaDo\LightSalesBundle\Entity\Payment $payment = null)
    {
        $this->payment = $payment;
    
        return $this;
    }

    /**
     * Get payment
     *
     * @return MikaDo\LightSalesBundle\Entity\Payment 
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set invoide
     *
     * @param MikaDo\LightSalesBundle\Entity\Invoice $invoide
     * @return Commande
     */
    public function setInvoide(\MikaDo\LightSalesBundle\Entity\Invoice $invoide = null)
    {
        $this->invoide = $invoide;
    
        return $this;
    }

    /**
     * Get invoide
     *
     * @return MikaDo\LightSalesBundle\Entity\Invoice 
     */
    public function getInvoide()
    {
        return $this->invoide;
    }
}