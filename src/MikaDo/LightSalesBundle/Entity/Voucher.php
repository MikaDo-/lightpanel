<?php

namespace MikaDo\LightSalesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightSalesBundle\Entity\Voucher
 *
 * @ORM\Table(name="lsales_voucher")
 * @ORM\Entity(repositoryClass="MikaDo\LightSalesBundle\Entity\VoucherRepository")
 */
class Voucher
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $code
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var integer $type
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;
    /*
     * 1: discount (en %)
     * 2: discount (en €)
     * 3: 1 free service
     */

    /**
     * @var integer $value
     *
     * @ORM\Column(name="value", type="float")
     */
    private $value;
    
    /**
     * @var integer $minimumPrice
     *
     * @ORM\Column(name="minimumPrice", type="float")
     */
    private $minimumPrice;

    /**
     * @var \DateTime $expiration
     *
     * @ORM\Column(name="expiration", type="datetime")
     */
    private $expiration;
    
    /**
     * @ORM\ManyToMany(targetEntity="MikaDo\LightSalesBundle\Entity\BuyableItem")
     */
    private $offeredItems;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Voucher
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Voucher
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set expiration
     *
     * @param \DateTime $expiration
     * @return Voucher
     */
    public function setExpiration($expiration)
    {
        $this->expiration = $expiration;
    
        return $this;
    }

    /**
     * Get expiration
     *
     * @return \DateTime 
     */
    public function getExpiration()
    {
        return $this->expiration;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->offeredItems = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set value
     *
     * @param float $value
     * @return Voucher
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Add offeredItems
     *
     * @param MikaDo\LightSalesBundle\Entity\BuyableItem $offeredItems
     * @return Voucher
     */
    public function addOfferedItem(\MikaDo\LightSalesBundle\Entity\BuyableItem $offeredItems)
    {
        $this->offeredItems[] = $offeredItems;
    
        return $this;
    }

    /**
     * Remove offeredItems
     *
     * @param MikaDo\LightSalesBundle\Entity\BuyableItem $offeredItems
     */
    public function removeOfferedItem(\MikaDo\LightSalesBundle\Entity\BuyableItem $offeredItems)
    {
        $this->offeredItems->removeElement($offeredItems);
    }

    /**
     * Get offeredItems
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getOfferedItems()
    {
        return $this->offeredItems;
    }

    /**
     * Set minimumPrice
     *
     * @param float $minimumPrice
     * @return Voucher
     */
    public function setMinimumPrice($minimumPrice)
    {
        $this->minimumPrice = $minimumPrice;
    
        return $this;
    }

    /**
     * Get minimumPrice
     *
     * @return float 
     */
    public function getMinimumPrice()
    {
        return $this->minimumPrice;
    }
}