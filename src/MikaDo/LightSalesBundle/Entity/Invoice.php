<?php

namespace MikaDo\LightSalesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightSalesBundle\Entity\Invoice
 *
 * @ORM\Table(name="lsales_invoice")
 * @ORM\Entity(repositoryClass="MikaDo\LightSalesBundle\Entity\InvoiceRepository")
 */
class Invoice
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\OneToOne(targetEntity="MikaDo\LightSalesBundle\Entity\Commande", mappedBy="invoice")
     */
    private $commande;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set commande
     *
     * @param MikaDo\LightSalesBundle\Entity\Commande $commande
     * @return Invoice
     */
    public function setCommande(\MikaDo\LightSalesBundle\Entity\Commande $commande = null)
    {
        $this->commande = $commande;
    
        return $this;
    }

    /**
     * Get commande
     *
     * @return MikaDo\LightSalesBundle\Entity\Commande 
     */
    public function getCommande()
    {
        return $this->commande;
    }
}