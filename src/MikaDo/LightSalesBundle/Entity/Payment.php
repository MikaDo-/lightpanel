<?php

namespace MikaDo\LightSalesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MikaDo\LightSalesBundle\Entity\Payment
 *
 * @ORM\Table(name="lsales_payment")
 * @ORM\Entity(repositoryClass="MikaDo\LightSalesBundle\Entity\PaymentRepository")
 */
class Payment
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $date;
    
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $mean;
    
    /**
     * @ORM\OneToOne(targetEntity="MikaDo\LightSalesBundle\Entity\Commande", inversedBy="payment")
     */
    private $commande;
    
    /**
     * @ORM\ManyToOne(targetEntity="MikaDo\LightUserBundle\Entity\User")
     */
    private $emitter;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Payment
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set mean
     *
     * @param string $mean
     * @return Payment
     */
    public function setMean($mean)
    {
        $this->mean = $mean;
    
        return $this;
    }

    /**
     * Get mean
     *
     * @return string 
     */
    public function getMean()
    {
        return $this->mean;
    }

    /**
     * Set emitter
     *
     * @param MikaDo\LightUserBundle\Entity\User $emitter
     * @return Payment
     */
    public function setEmitter(\MikaDo\LightUserBundle\Entity\User $emitter = null)
    {
        $this->emitter = $emitter;
    
        return $this;
    }

    /**
     * Get emitter
     *
     * @return MikaDo\LightUserBundle\Entity\User 
     */
    public function getEmitter()
    {
        return $this->emitter;
    }

    /**
     * Set commande
     *
     * @param MikaDo\LightSalesBundle\Entity\Commande $commande
     * @return Payment
     */
    public function setCommande(\MikaDo\LightSalesBundle\Entity\Commande $commande = null)
    {
        $this->commande = $commande;
    
        return $this;
    }

    /**
     * Get commande
     *
     * @return MikaDo\LightSalesBundle\Entity\Commande 
     */
    public function getCommande()
    {
        return $this->commande;
    }
}